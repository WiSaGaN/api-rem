# REM API

API for Shengli REM for linux x64

## Preprocessing of API files
API files usually come from web downloads from Shengli or future brokers in a compressed archive.
Files under go below preprocessing before committing to the repository:
* Convert encoding from GB2312 to UTF-8
* Convert end-of-line style from dos to unix
* Remove trailing white spaces
